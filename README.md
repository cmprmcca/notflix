How to start and test system.

1. Set IP of VM to 192.168.56.7
2.run "sudo docker-compose build"
3.Once system has built run "sudo docker-compose up"
4.There will be an error connecting to the RabbitMQ service
5.To get past this error on a web browser go to http://192.168.56.7:15672/
6.The RabbitMQ login user name is "user" and password "bitnami"
7.After logging in navigate to admin and make a new user called Test with the password Test as well as admin privliages
8.Once user is set up click on the user and set permissions
9.A message should appear on the VM console saying a node is waiting for a node response
10.After around 10 seconds all three nodes should send messages to each other
11.This is indicated by each node printing an array to the console containing the other two nodes hostnames, IDs and timestamps
12. A leadership election then occurs around 5 seconds after this with the three nodes printing if they are or aren't the leader
13.The leader is determined by whoever has the highest ID
14.After a minute each node will print a message saying whether or not the other two nodes are online
15.To test the failsafe press ctrl + z and type "sudo docker container stop node1" (node2 and node3 can also be used in this commmand)
16.After running this command type "fg" to go back to the foreground
17.After this container has been down for a full minute the next time the failsafe runs its check it will be picked up this conatainer is down
18.Once this runs the nodes will check if they are now the leader in case the leader has gone down. If a node already is the leader then it won't check
19.The leader will use the Docker API to start a new container to replace the one that has gone down and once this has been made the console will say "Container Start Completed"
20.This new container will then appear in the arrays of the other two containers as well as in the running containers if you run the command "sudo docker container ls
21.Note - the newly created nodes do not print messages to the console. However they can be seen appearing in the arrays of the other nodes and offer all the functionality of the other nodes.
