//Object data modelling library for mongo
const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

//connection string listing the mongo servers.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/notFLIXDB?replicaSet=cfgrs';

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var videoSchema = new Schema({
  AccountID: Number,
  UserName: String,
  TitleID: Number,
  UserAction: String,
  DateAndTime: String,
  PointOfInteraction: String,
  TypeOfInteraction: String
});

var videoModel = mongoose.model('Video', videoSchema, 'video');

app.get('/', (req, res) => {
  videoModel.find({},'item price quantity lastName', (err, video) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(video))
  }) 
})

app.post('/',  (req, res) => {
        var new_video_instance = new videoModel(req.body);
        new_video_instance.save(function (err) {
        if (err) res.send('Error');
        res.send(JSON.stringify(req.body))
        });
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

//Get the hostname of the node
var os = require("os");
var myhostname = os.hostname();

var nodeID = Math.floor(Math.random() * (1000000 - 1 + 1) + 1);
toSend = {"hostname" : myhostname, "status" : "alive", "nodeID" : nodeID};

var amqp = require('amqplib/callback_api');

setInterval(function() {
amqp.connect('amqp://test:test@192.168.56.7', function(error0, connection) {

if (error0) {
        throw error0;
      }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';
              var msg =  JSON.stringify(toSend);

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });
              channel.publish(exchange, '', Buffer.from(msg));
	      console.log(myhostname, "Sending Message");
            });
});
}, 10000);

let nodes = [];
amqp.connect('amqp://test:test@192.168.56.7', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(myhostname + " Waiting for Node response", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(msg) {
                                            if(msg.content) {
                                                        var stringObj = msg.content.toString();
						        var nodeObj = JSON.parse(stringObj);
						        if (nodeObj.hostname != myhostname) {
							nodes.some(node => node.hostname === nodeObj.hostname) ? (nodes.find(e => e.hostname === nodeObj.hostname)).timestamp = Date.now() : nodes.push({"hostname":nodeObj.hostname,"ID":nodeObj.nodeID,"timestamp":Date.now()});
                                                        console.log(nodes);
						        }
					    }
                                }, {
                                                      noAck: true
                                                    });
                              });
            });
});

var systemLeader = 0;
//check if leader
function leadershipElection() {
	systemLeader = 0;
  for (var i = 0; i < nodes.length; i++) {
  	if(nodes[i].ID > nodeID) {
		systemLeader = 0;
		break;
	}
	else {
		systemLeader = 1;
	}
  }
  console.log(myhostname + (systemLeader ? " LEADER:)":" NOT THE LEADER:("));
}
setInterval(leadershipElection, 15000);

//check for failure
setInterval(function() {
  var time = Date.now();
  for (var i = 0; i < nodes.length; i++) {
	if(time - nodes[i].timestamp > 60*1000) {
		console.log(nodes[i].hostname + " been down for 1 minute need to restart");
		nodes.splice(i, 1);
		console.log(nodes);
		if (!systemLeader) {
			leadershipElection();
		}
		if (systemLeader) {
			console.log("I MUST BRING NODE BACK ONLINE!!")
			startContainer();
		}
	} else {
		console.log(nodes[i].hostname + " is still online");
	}
  }
}, 60000);

//import the request libraryvar
request = require('request');

//This is the URL endopint of vm running dockervar
url = 'http://192.168.56.7:2375';

function startContainer() {
//create the post object to send to the docker api to create a container
var create = {
    uri: url + "/v1.40/containers/create",
	method: 'POST',
    //deploy an alpine container that runs echo hello world
	json: {"Image": "notflix_node1", "CMD": ["pm2-runtime", "mongo.js", "--watch" ] }
};

//send the create request
request(create, function (error, response, createBody) {
    if (!error) {
	    console.log("Created container " + JSON.stringify(createBody));

        //post object for the container start request
        var start = {
            uri: url + "/v1.40/containers/" + createBody.Id + "/start",
	      	method: 'POST',
	        json: {}
	    };

	    //send the start request
        request(start, function (error, response, startBody) {
	        if (!error) {
		        console.log("Container start completed");

                //post object for  wait
                var wait = {
			        uri: url + "/v1.40/containers/" + createBody.Id + "/wait",
                    method: 'POST',
		            json: {}
		        };


			    request(wait, function (error, response, waitBody ) {
			        if (!error) {
				        console.log("run wait complete, container will have started");

                        //send a simple get request for stdout from the container
                        request.get({
                            url: url + "/v1.40/containers/" + createBody.Id + "/logs?stdout=1",
                            }, (err, res, data) => {
                                    if (err) {
                                        console.log('Error:', err);
                                    } else if (res.statusCode !== 200) {
                                        console.log('Status:', res.statusCode);
                                    } else{
                                        //need to parse the json response to access
                                        console.log("Container stdout = " + data);
                                    }
                                });
                        }
		        });
            }
        });

    }
});
};
